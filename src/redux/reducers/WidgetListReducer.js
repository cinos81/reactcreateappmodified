import * as actionTypes from '../actionTypes';

/**
 * 위젯 리스트용 액션별 페이로드를 관리하는 리듀서를 생성하는 함수.
 *
 * @param state
 * @param action
 * @returns {Array}
 * @constructor
 */
export default function WidgetListState(state = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.WIDGET_LIST_INIT: {
      const { widgetListState } = action.payload;
      finalState = finalState.concat(widgetListState);
    } break;

    default: {
      finalState = finalState.concat(state);
    }
  }

  return finalState;
}

