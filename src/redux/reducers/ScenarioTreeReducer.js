import * as actionTypes from '../actionTypes';

export default function ScenarioTreeState(state = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.SCENARIO_TREE_INIT: {
      const { scenarioTreeState } = action.payload;
      finalState = finalState.concat(scenarioTreeState);
    } break;
    
    case actionTypes.SCENARIO_TREE_SELECT: {
      const { selectedNodeId } = action.payload;
      
    } break;

    default: {
      finalState = finalState.concat(state);
    }
  }

  return finalState;
}
