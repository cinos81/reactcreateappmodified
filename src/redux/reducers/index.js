import { combineReducers } from 'redux';
import WidgetListState from './WidgetListReducer';
import PageLayoutListState from './PageLayoutListReducer';
import ScenarioTreeState from './ScenarioTreeReducer';
import ScenarioFilter from './ScenarioFilter';

export default combineReducers({
  WidgetListState,
  PageLayoutListState,
  ScenarioTreeState,
  ScenarioFilter,
});
