import * as actions from '../actions';
import * as actionTypes from '../actionTypes';

/**
 * 현재 활성화된 시나리오 필터 액션을 리턴한다
 * @param state
 */
export default function ScenarioFilter(state = actions.selectedScenario, action) {
  let finalState;
  switch (action.type) {
    case actionTypes.SET_SCENARIO_FILTER:
      finalState = action.filter;
      break;
    default:
      finalState = state;
      break;
  }
  return finalState;
}
