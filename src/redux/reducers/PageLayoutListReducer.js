import * as actionTypes from '../actionTypes';

export default function PageLayoutList(state = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.WIDGET_POSITION_LIST_INIT: {
      const { layoutListState } = action.payload;
      finalState = finalState.concat(layoutListState);
    } break;

    default: {
      finalState = finalState.concat(state);
    }
  }

  return finalState;
}
