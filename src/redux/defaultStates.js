import * as widgets from '../Components/Widgets';

export const widgetListState = Object.keys(widgets);

export const layoutListState = [
  {
    scenarioId: 0,
    structure: [{
      row: 0,
      widgets: [{
        widgetType: 'PlaceHolder',
        size: 12,
      }],
    }, {
      row: 1,
      widgets: [{
        widgetType: 'PlaceHolder',
        size: 4,
      }, {
        widgetType: 'PlaceHolder',
        size: 8,
      }],
    }],
  }, {
    scenarioId: 4,
    structure: [{
      row: 1,
      widgets: [{
        widgetType: 'PlaceHolder',
        size: 6,
      }, {
        widgetType: 'PlaceHolder',
        size: 6,
      }],
    }, {
      row: 2,
      widgets: [{
        widgetType: 'PlaceHolder',
        size: 12,
      }],
    }],
  },
];

export const scenarioTreeState = [{
  name: '시나리오 목록 1',
  toggled: true,
  is_open: true,
  children: [{
    name: '시나리오 1',
    id: 2,
    selected: false,
  }, {
    name: '시나리오 2',
    id: 3,
    selected: false,
  }, {
    name: '시나리오 목록 3',
    toggled: true,
    is_open: true,
    children: [{
      name: '시나리오 4',
      id: 4,
      selected: true,
    }],
  }],
}, {
  name: '시나리오 목록 2',
  is_open: false,
  children: [{
    name: '시나리오 5',
    id: 5,
    selected: false,
  }],
}];
