import * as defaultStates from './defaultStates';
import * as actionTypes from './actionTypes';

/**
 * 위젯 리스트 초기화 액션
 * @param widgetList
 * @returns {{type: string, payload: {widgetList: Array}}}
 */
export function widgetListStateInitAction(widgetListState = defaultStates.widgetListState) {
  return {
    type: actionTypes.WIDGET_LIST_INIT,
    payload: { widgetListState },
  };
}

/**
 * 위젯 레이아웃 초기화 액션. 값이 전달되지 않으면 초기값으로 갈음한다.
 * @param positionList
 */
export
function widgetPositionListStateInitAction(layoutListState = defaultStates.layoutListState) {
  return {
    type: actionTypes.WIDGET_POSITION_LIST_INIT,
    payload: { layoutListState },
  };
}

/**
 * 시나리오 트리 초기화 액션. 값이 전달되지 않으면 초기값으로 갈음한다.
 * @param scenarioTree
 */
export function scenarioTreeStateInitAction(scenarioTreeState = defaultStates.scenarioTreeState) {
  return {
    type: actionTypes.SCENARIO_TREE_INIT,
    payload: { scenarioTreeState },
  };
}

/**
 * 시나리오 트리의 노드 선택 액션
 * @param selectedNodeId
 * @returns {{type, payload: {selectedNodeId: *}}}
 */
export function scenarioTreeSelectAction(selectedNodeId) {
  return {
    type: actionTypes.SCENARIO_TREE_SELECT,
    payload: { selectedNodeId },
  }
}

/**
 * 시나리오 필터를 리턴한다
 * @param filter
 * @returns {{type: string}}
 */
export function selectedScenario(filter) {
  return {
    type: actionTypes.SET_SCENARIO_FILTER,
    filter,
  };
}

export function selectedLayout(filter) {
  return {
    type: actionTypes.SET_SCENARIO_FILTER,
    filter,
  };
}
