import {
  createStore,
  applyMiddleware,
  compose,
} from 'redux';
import devTools from 'remote-redux-devtools';
import thunkMiddleWare from 'redux-thunk';
import reducers from './reducers';
import * as actions from './actions';

/* eslint-disable no-underscore-dangle */
class Store {
  constructor(reducers_, createStore_, applyMiddleware_, compose_, thunkMiddleWare_) {
    this._reducers = reducers_;
    this._createStore = createStore_;
    this._applyMiddleware = applyMiddleware_;
    this._compose = compose_;
    this._thunkMiddleWare = thunkMiddleWare_;

    this.init();
  }

  createFinalStore() {
    const {
      _reducers,
      _createStore,
      _applyMiddleware,
      _compose,
      _thunkMiddleWare,
    } = this;

    const finalCreateStore =
      _compose(_applyMiddleware(_thunkMiddleWare), this.applyReduxDevTool())(_createStore);
    this.store = finalCreateStore(_reducers);
  }

  init() {
    this.createFinalStore();

    this.store.dispatch(actions.widgetListStateInitAction());
    this.store.dispatch(actions.widgetPositionListStateInitAction());
    this.store.dispatch(actions.scenarioTreeStateInitAction());
  }

  applyReduxDevTool() {
    const isDevEnvironment = process.env.NODE_ENV === 'development';
    const isClient = typeof window === 'object';
    const reduxExtensionFound = typeof window.devToolsExtension !== 'undefined';
    return (isDevEnvironment && isClient && reduxExtensionFound) ?
      window.devToolsExtension() : f => f;
  }

  applyReduxRemoteDevTool() {
    const isDevEnvironment = process.env.NODE_ENV === 'development';
    return isDevEnvironment ? devTools() : f => f;
  }
}

export const store =
  new Store(reducers, createStore, applyMiddleware, compose, thunkMiddleWare).store;
