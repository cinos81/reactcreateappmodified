import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './redux';
import { SelectedMainPage } from './Containers/SelectedMainPage';

window.onload = () => {
  ReactDOM.render(
    <Provider store={store} >
      <SelectedMainPage />
    </Provider>,
    document.getElementById('root')
  );
};
