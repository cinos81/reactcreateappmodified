import React, { Component } from 'react';
import NVD3Chart from 'react-nvd3';
import './NVD3StackedArea.css';
import sampleData from './stackedAreaData.json';
import manifest from './manifest.json';
const thumbnail = require(`./${manifest.thumbnail}`);

export class NVD3StackedArea extends Component {
  static defaultProps = {
    datum: sampleData,
    xAxis: {
      tickFormat(d) {
        return window.d3.time.format('%Y/%m/%d')(new Date(d));
      },
    },
    yAxis: {
      tickFormat(d) {
        return d.toFixed(2);
      },
    },
    widgetName: manifest.widgetName,
    widgetId: manifest.widgetId,
    thumbnail,
  };

  static propTypes = {
    datum: React.PropTypes.array.isRequired,
    widgetName: React.PropTypes.string.isRequired,
    widgetId: React.PropTypes.string.isRequired,
  };

  render() {
    const { props } = this;

    return (
      <div className={props.className}>
        <div className="box box-solid">
          <div className="box-header">
            <i className="fa fa-bar-chart-o"></i>
            <h3 className="box-title">{props.widgetName}</h3>
            <div className="box-tools pull-right">
              <button
                type="button" className="btn btn-default btn-sm"
                data-widget="remove"
              >
                <i className="fa fa-times"></i>
              </button>
            </div>
          </div>
          <div className="box-body">
            <div className="row">
              <div className="col-md-12">
                <NVD3Chart
                  type="stackedAreaChart"
                  xAxis={props.xAxis}
                  yAxis={props.yAxis}
                  datum={props.datum}
                  x={(d) => d[0]}
                  y={(d) => d[1]}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
