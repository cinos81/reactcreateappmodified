import React, { Component } from 'react';
import $ from 'jquery';
import 'jquery-knob';
import manifest from './manifest.json';
const thumbnail = require(`./${manifest.thumbnail}`);

/**
 * 위젯 예제.
 * Knob차트가 구현되어 있음.
 */
export class KnobChart extends Component {
  /**
   * 초기 설정값. manifest.json에서 읽어들이는 형태.
   * @type {{widgetName: *, widgetId: *, thumbnail: *}}
   */
  static defaultProps = {
    widgetName: manifest.widgetName,
    widgetId: manifest.widgetId,
    thumbnail,
  };

  static propTypes ={
    widgetName: React.PropTypes.string.isRequired,
    widgetId: React.PropTypes.string.isRequired,
  };

  /**
   * 컴퍼넌트가 Dom에 마운트 된 이후 호출되는 메소드.
   * 이 시점에 jQuery로 작성된 Knob차트 플러그인을 실행시킨다.
   */
  componentDidMount() {
    $('.knob').knob();
  }

  /**
   * 컴퍼넌트 렌더링 함수.
   * @returns {XML}
   */
  render() {
    const { props } = this;

    return (
      <div className={props.className}>
        <div className="box box-solid">
          <div className="box-header">
            <i className="fa fa-bar-chart-o"></i>
            <h3 className="box-title">{props.widgetName}</h3>
            <div className="box-tools pull-right">
              <button
                type="button" className="btn btn-default btn-sm"
                data-widget="remove"
              >
                <i className="fa fa-times"></i>
              </button>
            </div>
          </div>
          <div className="box-body">
            <div className="row">
              <div className="col-xs-6 col-md-6 col-sm-6 text-center">
                <input
                  type="text" className="knob" defaultValue="30"
                  data-width="90" data-height="90" data-fgColor="#3c8dbc"
                  data-readonly="true"
                />
                <div className="knob-label">data-width="90"</div>
              </div>
              <div className="col-xs-6 col-md-6 col-sm-6 text-center">
                <input
                  type="text" className="knob" defaultValue="30" data-width="120"
                  data-height="120" data-fgColor="#f56954"
                />
                <div className="knob-label">data-width="120"</div>
              </div>
              <div className="col-xs-6 col-md-6 col-sm-6 text-center">
                <input
                  type="text" className="knob" defaultValue="30" data-thickness="0.1"
                  data-width="90" data-height="90" data-fgColor="#00a65a"
                />
                <div className="knob-label">data-thickness="0.1"</div>
              </div>
              <div className="col-xs-6 col-md-6 col-sm-6 text-center">
                <input
                  type="text" className="knob" data-thickness="0.2"
                  data-angleArc="250" data-angleOffset="-125" defaultValue="30"
                  data-width="120" data-height="120" data-fgColor="#00c0ef"
                />
                <div className="knob-label">data-angleArc="250"</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
