import React from 'react';
import { UIConfigureList, ScenarioTree, ScenarioLayoutRenderer } from '../../Common';
import './Resources/bootstrap/css/bootstrap.min.css';
import './Resources/dist/css/AdminLTE.min.css';
import './Resources/dist/css/skins/skin-blue.min.css';

export function MainPage(props) {
  return (
    <div className="wrapper">
      <header className="main-header">
        <a href="#" className="logo">
          <span className="logo-mini"><b>A</b>LT</span>
          <span className="logo-lg"><b>대시보드</b>편집기</span>
        </a>

        <nav className="navbar navbar-static-top" role="navigation">
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <li className="dropdown messages-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-envelope-o"></i>
                  <span className="label label-success">4</span>
                </a>
              </li>

              <li className="dropdown notifications-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-bell-o"></i>
                  <span className="label label-warning">10</span>
                </a>
                <ul className="dropdown-menu">
                  <li className="header">You have 10 notifications</li>
                  <li>
                    <ul className="menu">
                      <li>
                        <a href="#">
                          <i className="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <li className="dropdown tasks-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-flag-o"></i>
                  <span className="label label-danger">9</span>
                </a>
                <ul className="dropdown-menu">
                  <li className="header">You have 9 tasks</li>
                  <li>
                    <ul className="menu">
                      <li>
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small className="pull-right">20%</small>
                          </h3>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              <li className="dropdown user user-menu">
                <ul className="dropdown-menu">
                  <li className="user-body">
                    <div className="row">
                      <div className="col-xs-4 text-center">
                        <a href="#">Followers</a>
                      </div>
                      <div className="col-xs-4 text-center">
                        <a href="#">Sales</a>
                      </div>
                      <div className="col-xs-4 text-center">
                        <a href="#">Friends</a>
                      </div>
                    </div>
                  </li>
                  <li className="user-footer">
                    <div className="pull-left">
                      <a href="#" className="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div className="pull-right">
                      <a href="#" className="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#" data-toggle="control-sidebar"><i className="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <aside className="main-sidebar">
        <section className="sidebar">
          <UIConfigureList {...props} />
        </section>
        <section className="sidebar">
          <ScenarioTree {...props} id="scenariotree" data={props.ScenarioTreeState} />
        </section>
      </aside>
      <div className="content-wrapper">
        <section className="content-header">
          <h1>
            화면 1
            <small>위젯을 배치하십시오</small>
          </h1>
          <ol className="breadcrumb">
            <li><a href="#"><i className="fa fa-dashboard"></i> 단계</a></li>
            <li className="active">1.위젯 배치</li>
          </ol>
        </section>
        <ScenarioLayoutRenderer {...props} />
      </div>

      <footer className="main-footer">
        <div className="pull-right hidden-xs">
          Anything you want
        </div>
        <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
      </footer>
      <aside className="control-sidebar control-sidebar-dark">
        <ul className="nav nav-tabs nav-justified control-sidebar-tabs">
          <li className="active">
            <a href="#control-sidebar-home-tab" data-toggle="tab">
              <i className="fa fa-home"></i>
            </a>
          </li>
          <li>
            <a href="#control-sidebar-settings-tab" data-toggle="tab">
              <i className="fa fa-gears"></i>
            </a>
          </li>
        </ul>
        <div className="tab-content">
          <div className="tab-pane active" id="control-sidebar-home-tab">
            <h3 className="control-sidebar-heading">Recent Activity</h3>
            <ul className="control-sidebar-menu">
              <li>
                <a href="#">
                  <i className="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div className="menu-info">
                    <h4 className="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul>
            <h3 className="control-sidebar-heading">Tasks Progress</h3>
          </div>
          <div className="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
          <div className="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 className="control-sidebar-heading">General Settings</h3>
              <div className="form-group">
                <label className="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" className="pull-right" />
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div>
            </form>
          </div>
        </div>
      </aside>
      <div className="control-sidebar-bg"></div>
    </div>
  );
}
