import React, { Component } from 'react';
import $ from 'jquery';
import 'jqtree';
import './jqtree.css';
import './custom.css';
import { scenarioTreeState } from '../../../redux/defaultStates';
import PostOrderTree from 'tree-iterator';

export class ScenarioTree extends Component {
  static defaultProps = {
    data: scenarioTreeState,
  };

  componentDidMount() {
    this.setUpDraggable();
  }

  getEventHandlers() {
    const self = this;
    const { props } = self;

    return {
      'tree.click'($e) {
        const isParent = $e.node.children.length > 0;
        if (isParent) {
          $e.preventDefault();
          return;
        }
      },
      'tree.select'($e) {
        props.scenarioTreeSelectAction($e.node.id);
      },
      'tree.contextmenu'($e) {
        const { node } = $e;
        alert(node.name);
      },
    };
  }

  getTreeOption() {
    const { props } = this;

    return {
      data: props.data,
      selectable: true,
      autoOpen: false,
      dragAndDrop: true,
      buttonLeft: false,
      closedIcon: $('<i class="fa fa-arrow-circle-left"></i>'),
      openedIcon: $('<i class="fa fa-arrow-circle-down"></i>'),
      onCreateLi(node, $li) {
        // Add 'icon' span before title
        const isParent = node.children.length > 0;
        let iconElement = '<i class="fa fa-tachometer"></i>';
        if (isParent) {
          iconElement = '<i class="fa fa-circle-o"></i>';
        }

        const isSelected = node.selected === true;
        if (isSelected) {
          $li.addClass('jqtree-selected');
        }
        $li.find('.jqtree-title').before(iconElement);
      },
    };
  }

  setUpDraggable() {
    const { props } = this;
    $(`#${props.id}`).tree(this.getTreeOption()).bind(this.getEventHandlers());
  }

  toggleNode(targetNodeId, selected) {
    const { props } = this;
    const $scenariotree = $(`#${props.id}`);
    const targetNode = $scenariotree.tree('getNodesByProperty', 'id', targetNodeId)[0];
    $scenariotree.tree('updateNode', targetNode, {
      selected,
    });
  }

  render() {
    console.log('렌더!');
    const { props } = this;
    return (
      <div id={props.id} className="sidebar-menu" />
    );
  }
}
