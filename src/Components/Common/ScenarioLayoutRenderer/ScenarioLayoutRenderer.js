import React, { Component } from 'react';
import { PlaceHolder } from '../PlaceHolder';
import { Knob, NVD3MultiBar, NVD3StackedArea } from '../../Widgets';

/**
 * 시나리오별 위젯의 배치를 결정하는 컴퍼넌트
 */
export class ScenarioLayoutRenderer extends Component {
  render() {
    return (
      <section className="content">
        <div className="row">
          <PlaceHolder className="col-md-12" />
        </div>
        <div className="row">
          <PlaceHolder className="col-md-5" />
          <PlaceHolder className="col-md-7" />
        </div>
        <div className="row">
          <Knob className="col-md-12" />
        </div>
        <div className="row">
          <NVD3MultiBar className="col-md-5" />
          <NVD3StackedArea className="col-md-7" />
        </div>
      </section>
    );
  }
}
