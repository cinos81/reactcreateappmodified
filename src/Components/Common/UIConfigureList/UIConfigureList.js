import React, { Component } from 'react';
import { Accordion, Panel } from 'react-bootstrap';
import { WidgetListPane } from '../WidgetListPane';

export class UIConfigureList extends Component {
  render() {
    const { props } = this;
    return (
      <div className="row">
        <div className="col-sm-offset-1 col-sm-10 text-center">
          <Accordion>
            <Panel header="바탕 설정" eventKey="1">
              바탕 설정 구현 예정
            </Panel>
            <Panel header="위젯" eventKey="2">
              <WidgetListPane {...props} />
            </Panel>
          </Accordion>
        </div>
      </div>
    );
  }
}
