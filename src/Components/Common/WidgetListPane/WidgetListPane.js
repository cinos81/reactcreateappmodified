import React, { Component } from 'react';
import * as widgets from '../../Widgets';
import './WidgetListPane.css';
import { v4 } from 'uuid';
import $ from 'jquery';
import 'jquery-ui/ui/widgets/draggable';

export class WidgetListPane extends Component {
  componentDidMount() {
    this.setupDragSource();
  }

  componentDidUpdate() {
    this.setupDragSource();
  }

  setupDragSource() {
    $('div[data-widgetid]').draggable({
      helper: 'clone',
      start($e, ui) {
        $(this).addClass('invisible');
        $(ui.helper).addClass('drag');
      },
      stop() {
        $(this).removeClass('invisible');
      },
    });
  }

  renderWidgetThumbNails() {
    const { props } = this;
    return props.WidgetListState.map(widgetId => {
      const { thumbnail, widgetName } = widgets[widgetId].defaultProps;
      return (
        <div key={v4()} data-widgetid={widgetId} >
          <img className="widgetThumbNail" alt={widgetName} src={thumbnail} />
          <h5>{widgetName}</h5>
        </div>
      );
    });
  }

  render() {
    return (
      <div>{this.renderWidgetThumbNails()}</div>
    );
  }
}
