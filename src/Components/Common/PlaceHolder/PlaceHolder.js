import React, { Component } from 'react';
import $ from 'jquery';
import 'jquery-ui/ui/widgets/droppable';
import './placeholder.css';

export class PlaceHolder extends Component {
  componentDidMount() {
    this.setupDropSource();
  }

  componentDidUpdate() {
    this.setupDropSource();
  }

  setupDropSource() {
    const $widgetPlaceHolder = $(this.refs.placeholder);
    $widgetPlaceHolder.droppable({
      over() {
        $widgetPlaceHolder.addClass('dropzone');
      },
      out() {
        $widgetPlaceHolder.removeClass('dropzone');
      },
      drop($e, ui) {
        const widgetid = ui.draggable.data('widgetid');
        $widgetPlaceHolder.removeClass('dropzone')
          .find('.labelStyle').text(`${widgetid} 위젯 반영됨`);
      },
    });
  }

  render() {
    const { props } = this;

    return (
      <div className={props.className}>
        <div className="box placeholder" ref="placeholder">
          <h3 className="text-center labelStyle">위젯을 배치하세요</h3>
        </div>
      </div>
    );
  }
}
