import { actions } from '../../redux';

// eslint-disable-next-line
const getSelectedNode = (nodes, selected) => {
  if (nodes) {
    for (let i = 0; i < nodes.length; i++) {
      if (nodes[i].selected) {
        return nodes[i];
      }
      const found = getSelectedNode(nodes[i].children, selected);
      if (found) return Object.assign({}, found);
    }
  }
};

const iterateTreeNode = (nodes = []) => {
  if (nodes) {
    return nodes.forEach(node => {
      const { children } = node;
      if (children) {
        iterateTreeNode(children);
      } else {
        return node;
      }
    })
  } else {
    return nodes;
  }
};

const getSelectedScenario = (scenarios, filter) => {
  let finalState;
  switch (filter) {
    case actions.selectedScenario: {
      finalState = getSelectedNode(scenarios, true);
    } break;
    default: {
      finalState = {};
    }
  }
  return finalState;
};

const getSelectedWidgetPosition = (positionList, scenarioId, filter) => {
  let finalState;
  switch (filter) {
    case actions.selectedLayout: {
      finalState = positionList.find(
        positionInfo => positionInfo.scenarioId === scenarioId
      );
    } break;
    default: {
      finalState = {};
    }
  }
  return finalState;
};

export function mainSelector(state) {
  const {
    WidgetPositionListState,
    ScenarioTreeState,
    ScenarioFilter,
    selectedLayout,
  } = state;

  const targetScenarioState = getSelectedScenario(ScenarioTreeState, ScenarioFilter);
  const targetLayout = getSelectedWidgetPosition(
    WidgetPositionListState,
    targetScenarioState.id,
    selectedLayout,
  );

  return Object.assign({}, state, {
    targetScenarioState,
    targetLayout,
  });
}
