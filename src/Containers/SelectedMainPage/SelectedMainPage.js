import { MainPage } from '../../Components/Pages/MainPage';
import { connect } from 'react-redux';
import { actions } from '../../redux';
import { mainSelector } from './selector';

export const SelectedMainPage =
  connect(mainSelector, actions)(MainPage);
